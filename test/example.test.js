//example.test.js
const expect = require('chai').expect;
const assert = require('chai').expect;
const mylib = require('../src/mylib');
var should = require('should');

describe("Unit testing mylib.js", () => {

    let myvar = undefined;

    // runs after all test in this block
    after(() => console.log('After mylib tests'));

    it("Should return 2 when using sum funtion with parameters a=1, b=2", () => {
        const result_1 = mylib.add(1, 1);
        expect(result_1).to.equal(2);
    });

    it("Should return 1 when using sub funtion with parameters c=2, d=1", () => {
        const result_2 = mylib.sub(2,1);
        expect(result_2).to.equal(1);
    });

    // runs tasks before tests in this block
    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing');
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar');
    });

    it('Myvar schould exist', () => {
        should.exist(myvar);
    });

    // test will randomly fail
    // it('Random', () => expect(mylib.random()).to.above(0.5));
});