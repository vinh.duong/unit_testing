/**
 * This arrow funtion returns sum of two parameters.
 * @param {number} param1 this is the first parameter
 * @param {number} param2 this is the second parameter
 * @returns {number}
 */

const add = (param1, param2) => {
    const result = param1 + param2;
    return result;
};

/**
 * This is arrow funtion too. It is on single line, it has return statement.
 * Without curly brackets it does not require return keyworld.
 * @param {number} a first parameter 
 * @param {number} b second parameter
 * @returns 
 */

const subtract = (a, b) => a - b;

module.exports = {
    add,
    sub: subtract,
    random: () => Math.random(),
    arrayGen: () => [1, 2, 3]
};