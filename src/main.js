// src/main.js
const express = require("express");
const mylib = require("./mylib");
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = mylib.add(a, b); // calling the funtion from orther file
    res.send(sum.toString());
});

app.get('/sub', (req, res) => {
    const c = parseInt(req.query.c);
    const d = parseInt(req.query.d);
    const sub1 = mylib.sub(c, d);
    res.send(sub1.toString());
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
});